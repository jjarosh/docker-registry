# Docker Registry

Quickly stand up a Docker Registry for testing.

Includes *Portainer* for Docker management (the user name and password are
`admin` and `vagrant`).

## Requirements

- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](https://www.vagrantup.com/downloads.html)