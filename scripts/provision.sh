#!/bin/sh

#set -xe
set -e

echo "Privileged code"

echo "Update repositories"
apt-get update

echo "Install miscellaneous programs"
apt-get install -y unzip dos2unix make
